FROM maven:3.6.1-jdk-8-slim AS build
COPY . /usr/deneme
WORKDIR /usr/deneme
RUN mvn clean install -DskipTests

FROM openjdk:8-jre-alpine
COPY --from=build /usr/deneme/target/helloworld-0.0.2-SNAPSHOT.jar /usr/src/deneme/
WORKDIR /usr/src/deneme
EXPOSE 8080
CMD ["java", "-jar", "helloworld-0.0.2-SNAPSHOT.jar"]